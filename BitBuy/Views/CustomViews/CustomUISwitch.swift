//
//  CustomUISwitch.swift
//  BitBuy
//
//  Created by MSApps on 04/04/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

@IBDesignable
class CustomUISwitch: UISwitch {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        self.addTarget(self, action: #selector(self.switchValueDidChange), for: .valueChanged)
        self.clipsToBounds = true
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 16
        changeColor(isOn: self.isOn)
    }
    
    @objc func switchValueDidChange(sender:UISwitch!) {
        changeColor(isOn: sender.isOn)
    }
    
    func changeColor(isOn : Bool){
        if isOn{
            self.tintColor = UIColor.white
            self.thumbTintColor = UIColor(red: 114.0/255.0, green: 201.0/255.0, blue: 202.0/255.0, alpha: 1.0)
            self.layer.borderColor = UIColor(red: 114.0/255.0, green: 201.0/255.0, blue: 202.0/255.0, alpha: 1.0).cgColor
        }else{
            self.tintColor = UIColor.white
            self.thumbTintColor = UIColor(red: 255.0/255.0, green: 131.0/255.0, blue: 108.0/255.0, alpha: 1.0)
            self.layer.borderColor = UIColor(red: 255.0/255.0, green: 131.0/255.0, blue: 108.0/255.0, alpha: 1.0).cgColor
        }
    }

}
