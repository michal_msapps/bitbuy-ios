//
//  CircleUIImageView.swift
//  BitBuy
//
//  Created by MSApps on 04/04/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

@IBDesignable
class CircleUIImageView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        self.layer.cornerRadius = self.frame.width / 2;
        self.layer.masksToBounds = true
    }
 

}
