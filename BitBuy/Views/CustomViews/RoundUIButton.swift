//
//  RoundUIButton.swift
//  BitBuy
//
//  Created by MSApps on 22/03/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

@IBDesignable
class RoundUIButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
