//
//  ItemCollectionViewCell.swift
//  BitBuy
//
//  Created by MSApps on 28/03/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class SearchItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemPrice: UILabel!

    override func awakeFromNib() {
        self.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        self.layer.shadowRadius = 10.0
        self.layer.shadowOpacity = 0.15
    }
}
