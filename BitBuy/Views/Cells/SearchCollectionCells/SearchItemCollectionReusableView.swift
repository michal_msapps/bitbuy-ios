//
//  SerachItemCollectionViewHeaderViewCollectionReusableView.swift
//  BitBuy
//
//  Created by MSApps on 29/03/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class SearchItemCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    func setup(){
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        self.layer.shadowRadius = 20.0
        self.layer.shadowOpacity = 0.15
        self.layer.masksToBounds = false
        
        self.searchBar.isTranslucent = false
        self.searchBar.backgroundImage = UIImage()
        
        if let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField {
            textFieldInsideSearchBar.layer.borderWidth = 1
            textFieldInsideSearchBar.layer.cornerRadius = 10
            textFieldInsideSearchBar.layer.borderColor = UIColor(red: 103.0/255.0, green: 194.0/255.0, blue: 195.0/255.0, alpha: 0.22).cgColor
            textFieldInsideSearchBar.backgroundColor = .white
            textFieldInsideSearchBar.textColor = .black
        }
    }
    
}
