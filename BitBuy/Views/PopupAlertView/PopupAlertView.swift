//
//  PopupAlertView.swift
//  BitBuy
//
//  Created by MSApps on 22/03/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

enum PopupAlertViewType : Int {
    case alert = 0
    case error = 1
    case confirmation = 2
}

protocol PopupAlertViewDelegate : class {
    func negativeButtonPressed()
    func positiveButtonPressed()
    func popupAlertDismissed()
}

class PopupAlertView: UIView {
    
    weak var delegate:PopupAlertViewDelegate?
    
    @IBOutlet weak var topIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var buttonsContainer: UIStackView!    
    @IBOutlet weak var positiveButton: RoundUIButton!
    @IBOutlet weak var negativeButton: RoundUIButton!
    @IBOutlet weak var buttonsContainerHeight: NSLayoutConstraint!
    
    class func createPopupalert(with type : PopupAlertViewType , title : String , message : String) -> PopupAlertView {
        let popup = UINib(nibName: "PopupAlert", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PopupAlertView
        
        switch type {
        case .alert:
            popup.topIcon.image = #imageLiteral(resourceName: "Alert icon")
        case .error:
            popup.topIcon.image = #imageLiteral(resourceName: "Error icon")
        case .confirmation:
            popup.topIcon.image = #imageLiteral(resourceName: "Confirmed icon")
        }
        
        popup.titleLabel.text = title
        popup.messageLabel.text = message
        
        return popup
    }
    
    @IBAction func buttonAction(_ sender: UIButton!) {
        if let dele = self.delegate {
            if sender.tag == 1 {
                dele.negativeButtonPressed()
            }else{
                dele.positiveButtonPressed()
            }
        }
        
        
        self.dismiss()
    }
    
    func addPositiveButton(with text : String) -> PopupAlertView {
        buttonsContainerHeight.constant = 108
        
        self.positiveButton.isHidden = false
        self.positiveButton.setTitle(text, for: .normal)
        return self
    }
    
    func setNegativeButton(with text : String) -> PopupAlertView{
        self.negativeButton.setTitle(text, for: .normal)
        return self
    }
    
    func present(view : UIView){
        self.frame = view.frame
        view.addSubview(self)
    }
    
    func dismiss(){
        self.removeFromSuperview()
    }
    
    func setDelegate(dele : PopupAlertViewDelegate) -> PopupAlertView {
        self.delegate = dele
        return self
    }
    
}
