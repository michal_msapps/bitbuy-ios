//
//  ProfileTableViewController.swift
//  BitBuy
//
//  Created by MSApps on 29/03/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit
import RxSwift

private let reuseIdentifier = "ProfileCell"

class ProfileTableViewController: BasicTableViewController {
    
    let bag = DisposeBag()
    var goToTab : Variable<MainTabBarTab>?
    var selectedTab : Observable<MainTabBarTab>? {
        return goToTab?.asObservable() ?? nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.view.frame.height / 7)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let variable = goToTab else {
           return
        }
        
        switch indexPath.row {
        case 0:
            // EditProfile
            
            performSegue(withIdentifier: "GoToEditProfile", sender: self)
            break
        case 1:
            // My Items
            //chnage tabbar selected tab to my items
            variable.value = .myItems
            
            self.navigationController?.popViewController(animated: true)
            break
        case 2:
            // My Candancy
            //chnage tabbar selected tab to my candancy
            variable.value = .myCandancy
            
            self.navigationController?.popViewController(animated: true)
            break
        case 3:
            // Settings
            performSegue(withIdentifier: "GoToSettings", sender: self)
            break
        case 4:
            // Logout
            //perform logout user && change tabbar selected tab to search
            self.navigationController?.dismiss(animated: true, completion: nil)
            break
        default:
            return
        }
        
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
 

}
