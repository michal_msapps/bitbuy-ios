//
//  LoginViewController.swift
//  BitBuy
//
//  Created by MSApps on 26/04/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class LoginViewController: BasicViewController {

    @IBOutlet weak var titlesTopConstaint: NSLayoutConstraint!
    @IBOutlet weak var otherLoginButtonsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var orLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var googleBtn: RoundUIButton!
    @IBOutlet weak var facbookBtn: RoundUIButton!
    @IBOutlet weak var nextBtn: RoundUIButton!
    
    var generalTopConstraintSpace : CGFloat {
        get{
            return (self.view.frame.height * 3.64842) / 100
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustConstraints()
        adjustButtons()
    }
    
    override func viewDidLayoutSubviews() {
        if googleBtn.borderColor == 0{
            
        }
    }
    
    func adjustButtons(){
        googleBtn.cornerRadius = googleBtn.frame.height / 2
        facbookBtn.cornerRadius = googleBtn.cornerRadius
        nextBtn.cornerRadius = googleBtn.cornerRadius
    }
    
    func adjustConstraints(){
        titlesTopConstaint.constant = generalTopConstraintSpace
        otherLoginButtonsTopConstraint.constant = generalTopConstraintSpace
        orLabelTopConstraint.constant = generalTopConstraintSpace
        textFieldTopConstraint.constant = generalTopConstraintSpace
        nextButtonTopConstraint.constant = generalTopConstraintSpace
        signupButtonTopConstraint.constant = generalTopConstraintSpace
    }

}
