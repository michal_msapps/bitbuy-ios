//
//  BasicTabViewController.swift
//  BitBuy
//
//  Created by MSApps on 26/04/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class BasicViewController: UIViewController{
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationBar()
    }

}
