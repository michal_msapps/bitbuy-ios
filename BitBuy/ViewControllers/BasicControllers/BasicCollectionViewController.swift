//
//  BasicCollectionViewController.swift
//  BitBuy
//
//  Created by MSApps on 26/04/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class BasicCollectionViewController: UICollectionViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    override func shouldHideNavBarShadow() -> Bool {
        return true
    }

}
