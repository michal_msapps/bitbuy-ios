//
//  tViewController.swift
//  BitBuy
//
//  Created by MSApps on 28/03/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit
import RxSwift

enum MainTabBarTab : Int{
    case search = 0
    case myItems = 1
    case addItem = 2
    case notificatinos = 3
    case myCandancy = 4
}

class TabBarController: UITabBarController , UITabBarControllerDelegate{
    
    private var currentSelectedTab : MainTabBarTab = .search
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        setupNavBar()
        setupTabBar()
    }
    
    func setupNavBar(){
        guard let navBar = self.navigationController?.navigationBar else{
            return
        }
        
        navBar.isTranslucent = false
        navBar.barStyle = .black
        navBar.layer.shadowColor = UIColor.black.cgColor
        navBar.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        navBar.layer.shadowRadius = 20.0
        navBar.layer.shadowOpacity = 0.15
        navBar.layer.masksToBounds = false
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    func setupTabBar(){
        self.delegate = self
        
        tabBar.unselectedItemTintColor = UIColor(red: 178.0/255.0, green: 178.0/255.0, blue: 178.0/255.0, alpha: 1.0)
        tabBar.backgroundColor = .white
        tabBar.tintColor = UIColor(red: 161.0/255.0, green: 194.0/255.0, blue: 195.0/255.0, alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the selected tab
        
        self.switchToTab(at:currentSelectedTab.rawValue)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToProfile"{
            let profileVC = segue.destination as! ProfileTableViewController
            profileVC.goToTab = Variable(currentSelectedTab)
            profileVC.selectedTab?.subscribe(onNext: { value in
                self.currentSelectedTab = value
            }).disposed(by: disposeBag)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return !(viewController is AddItemViewController)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let tab = MainTabBarTab(rawValue: tabBarController.selectedIndex){
          currentSelectedTab = tab
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func profileButtonPressed(_ sender: UIBarButtonItem) {
        
        let user = false
        var segue = ""
        
        segue = user ? "GoToProfile" : "GoToLogin"
        
        self.performSegue(withIdentifier: segue, sender: self)
        
    }
    
}


