//
//  Utils.swift
//  BitBuy
//
//  Created by MSApps on 29/03/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

protocol BasicNavigationBarShadowProtocol {
    func shouldHideNavBarShadow() ->Bool
}

extension UINavigationBar{
    func showNavigationShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
    }
    
    func hideNavigationShadow(){
        self.layer.shadowColor = UIColor.clear.cgColor
    }
}

extension UIViewController : BasicNavigationBarShadowProtocol{
    
    func setupNavigationBar(){
        if let navBar = self.navigationController?.navigationBar{
            
            self.shouldHideNavBarShadow() ? navBar.hideNavigationShadow() : navBar.showNavigationShadow()
            
            if let title = self.title{
                self.setTitle(with: title, preferedLarge: true)
            }
        }
    }
    
    func setTitle(with text : String , preferedLarge : Bool){
        if let NB = self.navigationController?.navigationBar {
            NB.topItem?.title = text
        }
    }
    
    @objc func shouldHideNavBarShadow() ->Bool {
        return false
    }
}

extension UITabBarController{
    func switchToTab(at index:Int){
        self.selectedViewController = self.viewControllers?[index]
    }
}
