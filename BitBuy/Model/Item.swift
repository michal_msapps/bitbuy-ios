//
//  Item.swift
//  BitBuy
//
//  Created by MSApps on 04/04/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

struct Item {
    let userId : String
    let name : String
    let description : String
    let price : NSNumber
    let picture : String
    let date : NSNumber
}
